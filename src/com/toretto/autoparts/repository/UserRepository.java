package com.toretto.autoparts.repository;

import com.toretto.autoparts.entity.Customer;

public class UserRepository {

	private static UserRepository userRepository;
	
	private Customer systemCustomer;
	
	private UserRepository() {
		systemCustomer = new Customer();
		systemCustomer.setId(1);
		systemCustomer.setEmail("user.test@mail.com");
		systemCustomer.setName("User Test");
		systemCustomer.setPassword("12345");
	}
	
	public static UserRepository getInstance(){
		if(userRepository == null){
			userRepository = new UserRepository();
		}
		return userRepository;
	}
	
	public Customer validateUser(Customer customer){
		if(customer.getEmail().equals(systemCustomer.getEmail())
				&& customer.getPassword().equals(systemCustomer.getPassword())){
			return systemCustomer;
		}
		return null;
	}
}
