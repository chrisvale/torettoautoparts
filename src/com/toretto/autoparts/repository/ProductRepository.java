package com.toretto.autoparts.repository;

import java.util.ArrayList;
import java.util.List;

import com.toretto.autoparts.entity.Product;

public class ProductRepository {
	
	private static ProductRepository productRepository;
	
	private ProductRepository(){
	}
	
	/**
	 * Singleton for ProductRepository
	 * @return
	 */
	public static ProductRepository getInstance(){
		if(productRepository==null){
			productRepository = new ProductRepository();
		}
		return productRepository;
	}
	
	private List<Product> products = new ArrayList<Product>();
	
	public void includeProduct(Product p){
		this.getProducts().add(p);
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
