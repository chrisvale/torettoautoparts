package com.toretto.autoparts.entity;

import java.io.Serializable;

public class PurchaseItem implements Serializable{
	
	private Product product;
	private Float unitPrice;
	private Integer quantity;
	private Float totalPrice =  0F;
	
	public PurchaseItem() {
		// TODO Auto-generated constructor stub
	}
	
	public PurchaseItem(Product product) {
		this.product = product;
	}
	
	public PurchaseItem(Product product, Integer quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public void calculateTotal() {
		totalPrice = product.getPrice() * quantity;
	    }
	   
    public void updateQuantity(Integer newQuantity) {
      this.quantity = newQuantity;
      calculateTotal();
    }
		
	public Float getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	

}
