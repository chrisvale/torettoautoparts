package com.toretto.autoparts.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Purchase implements Serializable {

	private Integer id;
	private Date date;
	private Float total;
	private Customer customer;
	private Set<PurchaseItem> itens;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setItens(Set<PurchaseItem> itens) {
		this.itens = itens;
	}

	public Set<PurchaseItem> getItens() {
		if (itens == null) {
			itens = new HashSet<PurchaseItem>();
		}
		return itens;
	}

	public List<PurchaseItem> getOrderedItens() {
		return new ArrayList<PurchaseItem>(getItens());
	}

	public void addItem(Product product, Integer quantity) {
		PurchaseItem itemExistente = getItem(product);
		if (itemExistente != null) {
			updateQuantity(product, itemExistente.getQuantity() + quantity);
		} else {
			getItens().add(new PurchaseItem(product, quantity));
			calculateTotal();
		}
	}

	public void removeItem(PurchaseItem product) {
		getItens().remove(product);
		calculateTotal();
	}

	public PurchaseItem getItem(Product product) {
		PurchaseItem itemAProcurar = new PurchaseItem(product);
		for (PurchaseItem item : getItens()) {
			if (item.getProduct().getId().equals((itemAProcurar.getProduct().getId()))) {
				return item;
			}
		}
		return null;
	}

	public void updateQuantity(Product product, Integer newQuantity) {
		PurchaseItem item = getItem(product);
		if (item == null) {
			throw new IllegalArgumentException("Item not found " + product);
		}
		item.updateQuantity(newQuantity);
		calculateTotal();
	}

	public void calculateTotal() {
		total = 0F;
		for (PurchaseItem item : getItens()) {
			item.calculateTotal();
			total += item.getTotalPrice();
		}
	}

}
