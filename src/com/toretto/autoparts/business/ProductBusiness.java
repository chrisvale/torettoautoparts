package com.toretto.autoparts.business;

import java.util.List;

import com.toretto.autoparts.entity.Product;
import com.toretto.autoparts.repository.ProductRepository;

public class ProductBusiness {
	
	private static ProductBusiness productBusiness;
	
	private ProductRepository productRepository = ProductRepository.getInstance();
	
	private ProductBusiness(){
	}
	
	/**
	 * Singleton for ProductBusiness
	 * @return
	 */
	public static ProductBusiness getInstance(){
		if(productBusiness==null){
			productBusiness = new ProductBusiness();
		}
		return productBusiness;
	}
	
	public List<Product> getProductList(){
		return productRepository.getProducts();
	}
	
	/**
	 * Mock the persistence persistence as a Fake list of products.
	 */
	public void includeProducts(){
		if(productRepository.getProducts().isEmpty()){
			productRepository.includeProduct(new Product(1, 
					"Seat Cover", 
					"Rockport black and grey truck seat cover, Part Number: 804658 Not Vehicle Specific Auto "
					+ "Expressions/Rockport black and grey truck seat cover", 
					new Float(100.00)));
			
			productRepository.includeProduct(new Product(2, 
					"Floor Mat", 
					"Protect your vehicle's flooring while showing your team pride with car mats by FANMATS. "
					+ "100% nylon face with non-skid vinyl backing. Universal fit makes it ideal for cars, trucks, "
					+ "SUV's and RV's. The officially licensed mat is chromojet printed in true team colors and designed "
					+ "with a large team logo. Made in USA.", 
					new Float(100.00)));
			
			productRepository.includeProduct(new Product(3, 
					"wheel cover", 
					"Our ABS plastic replica wheel covers use quality materials to maintain durability which is tolerant of "
					+ "extreme temperatures. It s easy to install and easy to clean. The stylish and trendy design will give "
					+ "your wheels a refreshing and fancy look.", 
					new Float(50.00)));
			
			productRepository.includeProduct(new Product(4, 
					"Engine oil", 
					"If you have over 75,000 miles, it�s time to step up the fight against engine breakdown. Switch to MaxLife Motor "
					+ "Oil, the synthetic blend with more!", 
					new Float(70.00)));
			
			
			productRepository.includeProduct(new Product(5, 
					"FlowTech/Terminator exhaust tip", 
					"Terminator� Exhaust Tips, 2.5 Dual Outlet / Slash Cut Design, Fits 2 Tailpipe, Overall Length 15, "
					+ "Polished Triple Chrome Plating.", 
					new Float(35.00)));
			
			
			productRepository.includeProduct(new Product(6, 
					"ProElite/Stainless steel exhaust tip - Bolt-on", 
					"Pro elite bolt-on stainless steel exhaust tip uses a bolt-on design featuring a multiple point clamping system for a secure installation."
					+ "High quality polished finish"
					+ " Stainless steel construction for durability"
					+ " Three bolt clamping system offers maximum security"
					+ " Quick and easy to install", 
					new Float(32.00)));
			
			productRepository.includeProduct(new Product(7, 
					"Edelbrock/15", 
					"extended and 20.95 in. collapsed zinc remote mount medium valve Xtreme Travel universal remote reservoir shock", 
					new Float(75.00)));
			
			productRepository.includeProduct(new Product(8, 
					"Coil Spring", 
					"Heavy Duty best ride. Fits most Heavy Duty shock absorbers. Protected with a heavy enamel coating.", 
					new Float(53.00)));
			
			productRepository.includeProduct(new Product(9, 
					"Covercraft/Ready-Fit car cover", 
					"Covercraft s line of Wolf Ready-Fit Car Covers are quality covers without the high cost of a custom fit cover. Made from the "
					+ "same excellent fabric lines their custom fit covers are made from. Fabrics are made with technology that is a barrier to "
					+ "moisture keeping it away from your vehicle, yet allows condensation or vapors to escape away from your vehicle. Protects "
					+ "your vehicle from all the various weather elements and provides protection against dents and dings that may occur while your "
					+ "vehicle is being stored. Covercraft s Wolf Ready-Fit Car Covers provide excellent UV-ray protection as well as an excellent "
					+ "dust and pollution barrier.", 
					new Float(32.00)));

			productRepository.includeProduct(new Product(10, 
					"Clutch Pressure Plate", 
					"Long-lasting silicone springs. Welded spring cups. Ductile iron pressure ring. Explosion resistant. Adjustable levers. Static pressure 2000-2600 lbs", 
					new Float(500.00)));
			
			productRepository.includeProduct(new Product(11, 
					"Cylinder Head - Performance", 
					"Out-of-the-box bolt-on performance, hand-blended intake and exhaust bowls, CNC matched intake/exhaust ports in std location, thicker deck surfaces than "
					+ "other brands, 54cc dual-quench combustion chamber, offers improved mid to high lift flow", 
					new Float(800.00)));
			
			productRepository.includeProduct(new Product(12, 
					"Timing Set - Performance", 
					"Proform/High performance timing gear drive set for Chevy big block includes locking plate, offset cam bushings, roller cam bearing, and bronze thrust bearing", 
					new Float(75.00)));
			productRepository.includeProduct(new Product(13, 
					"Clutch Disc - Performance", 
					"Super strength lining material. Aluminum backing. Custom forged center hub. 8 torque absorbing springs. Heat-treated backing plate. 8-pin drive design for strength.", 
					new Float(300.00)));
			productRepository.includeProduct(new Product(14, 
					"Transfer Case Service Parts", 
					"Mile Marker/Outer flange for Mopar 203 N.P. transfer case 602", 
					new Float(1000.00)));
		}
		
	}

}
