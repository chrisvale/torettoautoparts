package com.toretto.autoparts.business;

import java.util.Date;

import com.toretto.autoparts.entity.Customer;
import com.toretto.autoparts.entity.Purchase;

public class PurchaseBusiness {
	
	private static PurchaseBusiness purchaseBusiness;
	
	private PurchaseBusiness() {
	}
	
	/**
	 * Singleton for PurchaseBusiness
	 * @return
	 */
	public static PurchaseBusiness getInstance(){
		if(purchaseBusiness == null){
			purchaseBusiness = new PurchaseBusiness();
		}
		return purchaseBusiness;
	}
	
	public Purchase createPurchase(Purchase purchase, Customer customer){
		
		purchase.setCustomer(customer);
		purchase.setDate(new Date());
		purchase.setId(generateId());
				
		return purchase;		
	}
	
	private Integer generateId(){
		Double regId = Math.random();
		regId*=1000;
		Integer id = regId.intValue();
		return id;
	}

}
