package com.toretto.autoparts.business;

import com.toretto.autoparts.entity.Customer;
import com.toretto.autoparts.repository.UserRepository;

public class UserBusiness {
	
	private static UserBusiness userBusiness;
	
	private UserBusiness() {
	}
	
	/**
	 * Singleton for UserBusiness
	 * @return
	 */
	public static UserBusiness getInstance(){
		if(userBusiness==null){
			userBusiness = new UserBusiness();
		}
		return userBusiness;
	}
	
	public Customer validateUser(Customer customer){
		return UserRepository.getInstance().validateUser(customer);
	}
}
