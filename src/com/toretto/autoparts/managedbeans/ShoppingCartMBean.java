package com.toretto.autoparts.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import com.toretto.autoparts.business.PurchaseBusiness;
import com.toretto.autoparts.entity.Customer;
import com.toretto.autoparts.entity.Product;
import com.toretto.autoparts.entity.Purchase;
import com.toretto.autoparts.entity.PurchaseItem;

@ManagedBean
@SessionScoped
public class ShoppingCartMBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Purchase purchaseCart;
	private Integer idGeneratedPurchase;
	private Customer customer;
	private PurchaseItem productToRemove;
	private static final String SHOPPING_CART = "shoppingCart";
	private static final String CLOSED_PURCHASE = "closedPurchase";
	
	@Inject
    private transient UtilsMB utilsMB;
	
	@PostConstruct
    public void init() {
		purchaseCart = new Purchase();
		customer = this.getCustomer();
    }
	
	public String addToShoppingCart(Product product){
		purchaseCart.addItem(product, 1);
		return SHOPPING_CART;
	}

	public void removeFromCart(){
		purchaseCart.removeItem(productToRemove);
	}
	
	public void updateItemQuantity(Product product, Integer newQuantity){
		purchaseCart.updateQuantity(product, newQuantity);
	}
	   
    public String requestPurchase() {
        Purchase purchaseCreated = PurchaseBusiness.getInstance().createPurchase(purchaseCart,customer);
        idGeneratedPurchase = purchaseCreated.getId();
        
        purchaseCart = new Purchase();
        return CLOSED_PURCHASE;
    }
	   
    public void recalculateTotal(PurchaseItem item) {
    	item.calculateTotal();
    	purchaseCart.calculateTotal();
    }
	   
    public boolean hasItens() {
      return purchaseCart.getItens().size() > 0;
    }
    
    private Customer recoverCustomerFromSession(){
    	FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
		return (Customer) session.getAttribute(LoginMBean.LOGED_CUSTOMER);
    }

	public Purchase getPurchaseCart() {
		return purchaseCart;
	}

	public void setPurchaseCart(Purchase purchaseCart) {
		this.purchaseCart = purchaseCart;
	}

	public Integer getIdGeneratedPurchase() {
		return idGeneratedPurchase;
	}

	public void setIdGeneratedPurchase(Integer idGeneratedPurchase) {
		this.idGeneratedPurchase = idGeneratedPurchase;
	}

	public Customer getCustomer() {
		if(customer == null){
			customer = this.recoverCustomerFromSession();
		}
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public PurchaseItem getProductToRemove() {
		return productToRemove;
	}

	public void setProductToRemove(PurchaseItem productToRemove) {
		this.productToRemove = productToRemove;
	}

	public UtilsMB getUtilsMB() {
		return utilsMB;
	}

	public void setUtilsMB(UtilsMB utilsMB) {
		this.utilsMB = utilsMB;
	}
    
}
