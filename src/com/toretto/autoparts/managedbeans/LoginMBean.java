package com.toretto.autoparts.managedbeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.toretto.autoparts.business.ProductBusiness;
import com.toretto.autoparts.business.UserBusiness;
import com.toretto.autoparts.entity.Customer;

@ManagedBean
@RequestScoped
public class LoginMBean {
	
	private static final String PRODUCT = "product";
	public static final String LOGED_CUSTOMER = "logedCustomer";
	
	private Customer customer;
	
	@PostConstruct
	public void init(){
		customer = new Customer();
		customer.setEmail("user.test@mail.com");
		customer.setPassword("12345");
	}
	
	public String login(){
		Customer loguedUser = UserBusiness.getInstance().validateUser(customer);
		
		if(loguedUser == null){
			return null;
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		session.setAttribute(LOGED_CUSTOMER, loguedUser);
		
		getProductBusiness().includeProducts();
		
		return PRODUCT;
	}


	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public ProductBusiness getProductBusiness() {
		return ProductBusiness.getInstance();
	}

}
