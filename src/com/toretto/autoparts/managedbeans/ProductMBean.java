package com.toretto.autoparts.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import com.toretto.autoparts.business.ProductBusiness;
import com.toretto.autoparts.entity.Product;

@ManagedBean
@RequestScoped
public class ProductMBean {

	private Product product = new Product();
	private List<Product> productList;
	
	@Inject
	private ProductBusiness productBusiness;
	
	@PostConstruct
	private void getProducts(){
		productList = getProductBusiness().getProductList();
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public List<Product> getProductList() {
		return  productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
	
	public ProductBusiness getProductBusiness() {
		if(productBusiness == null){
			productBusiness = ProductBusiness.getInstance();
		}
		return productBusiness;
	}

}
